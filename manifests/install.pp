# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include planner::install
class planner::install {
  package { $planner::package_name:
    ensure => $planner::package_ensure,
  }
}
